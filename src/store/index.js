import { configureStore } from "@reduxjs/toolkit";
import mainReducer from "./slices";

export * from "./selectors";
export * from "./slices";
export * from "./thunk";

export default configureStore({
  reducer: {
    main: mainReducer,
  },
});
