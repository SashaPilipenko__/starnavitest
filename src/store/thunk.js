import { actions } from "./slices";
import Api from "utils/api";
import config from "_config";

const { setModes, getModesRequest, getModesError } = actions;

export const getModesAsync = () => async (dispatch) => {
  try {
    dispatch(getModesRequest());
    const data = await Api.get(config.api);
    dispatch(setModes(data));
  } catch {
    dispatch(getModesError());
  }
};
