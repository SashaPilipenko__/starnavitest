import { createDraftSafeSelector } from "@reduxjs/toolkit";

const selectDomain = (state) => state.main;

export const selectModes = createDraftSafeSelector(selectDomain, (substate) =>
  Object.keys(substate.modes)
);
