import { createSlice } from "@reduxjs/toolkit";

export const mainSlice = createSlice({
  name: "main",
  initialState: {
    modes: {},
    activeMode: {},
    size: 0,
    reports: [],
    loading: true,
  },
  reducers: {
    getModesRequest: (state) => {
      state.loading = true;
    },
    getModesError: (state) => {
      state.loading = false;
    },
    setModes: (state, { payload }) => {
      state.loading = false;
      state.modes = payload;
    },
    setActiveMode: (state, { payload }) => {
      state.activeMode = state.modes[payload];
    },
    startGame: (state) => {
      state.size = state.activeMode.field;
      state.reports = [];
    },
    setReport: (state, { payload }) => {
      state.reports.unshift(payload);
    },
  },
});

export const { actions } = mainSlice;

export default mainSlice.reducer;
