import styled from "styled-components";

export const Wrap = styled.div`
  height: 100vh;
`;

export const Select = styled.select`
  min-width: 280px;
`;

export const Button = styled.button`
  padding: 10px;
  border: none;
  background: lightblue;
  border-radius: 5px;
`;

export const Square = styled.div`
  width: 40px;
  height: 40px;
  border: 1px solid black;
  cursor: pointer;
  transition: 0 background-color;
  &:hover {
    transition-delay: 0.08s;
    background-color: lightblue;
  }
`;

export const FieldWrap = styled.div`
  min-width: 480px;
`;

export const ReportWrap = styled.div`
  max-height: 300px;
  overflow: hidden;
`;

export const SquareWrap = styled.div`
  ${(props) => props.size > 0 && "min-height: 300px"};
`;

export const ReportItem = styled.div`
  padding: 10px 15px;
  border-radius: 10px;
  margin-top: 7px;
  width: 100%;
  border: 1px solid yellow;
  background: lightyellow;
`;
