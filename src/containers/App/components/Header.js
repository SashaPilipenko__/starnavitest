import React from "react";
import { useDispatch } from "react-redux";
import { actions } from "store";

import { Select, Button } from "../Styled";

export function Header({ modes }) {
  const dispatch = useDispatch();

  const setMode = () => (e) => {
    dispatch(actions.setActiveMode(e.target.value));
  };

  const startGame = () => {
    dispatch(actions.startGame());
  };

  return (
    <div className="d-flex">
      <>
        <Select defaultValue={"Pick option"} onChange={setMode()}>
          <option disabled>Pick option</option>
          {modes.map((i) => {
            return (
              <option key={i} value={i}>
                {i}
              </option>
            );
          })}
        </Select>
        <Button onClick={startGame} className="ml-2">
          Start
        </Button>
      </>
    </div>
  );
}
