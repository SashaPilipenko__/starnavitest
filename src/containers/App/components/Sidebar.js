import React from "react";
import { NextId } from "utils/help-func";
import { ReportWrap, ReportItem } from "../Styled";

export function Sidebar({ reports }) {
  return (
    <ReportWrap className="col-3">
      <h3>History</h3>
      {reports.map(({ row, col }) => (
        <ReportItem key={NextId()}>{`row:${row},col:${col}`}</ReportItem>
      ))}
    </ReportWrap>
  );
}
