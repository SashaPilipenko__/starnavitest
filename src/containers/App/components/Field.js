import React, { useRef, memo } from "react";
import { useDispatch } from "react-redux";
import debounce from "lodash.debounce";
import { NextId } from "utils/help-func";
import { actions } from "store";

import { Square, SquareWrap } from "../Styled";

function FieldMemo({ size }) {
  const dispatch = useDispatch();

  const setHover = (e) => {
    dispatch(actions.setReport({ ...e.target.dataset }));
  };

  const setHoverDebounced = useRef(debounce(setHover, 80)).current;

  const mouseWorker = (e) => {
    if (e.target.dataset.row) {
      setHoverDebounced(e);
    }
  };

  let body = makeField(size);
  return (
    <SquareWrap size={size} className="d-flex flex-column col-9">
      {body.map((item) => (
        <div key={NextId()} className="d-flex">
          {item.map((i) => (
            <Square
              key={NextId()}
              onMouseEnter={mouseWorker}
              size={size}
              data-col={i.col}
              data-row={i.row}
            >
              {i.col}
            </Square>
          ))}
        </div>
      ))}
    </SquareWrap>
  );
}

export const Field = memo(FieldMemo);

function makeField(size) {
  let body = [];

  for (let i = 1; i <= size; i++) {
    body.push([]);
    for (let j = 1; j <= size; j++) {
      body[i - 1].push({ row: i, col: j });
    }
  }
  return body;
}
