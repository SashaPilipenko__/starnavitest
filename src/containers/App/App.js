import React, { useEffect, memo } from "react";
import { useSelector, useDispatch } from "react-redux";

import { getModesAsync, selectModes } from "store";
import { Wrap, FieldWrap } from "./Styled";
import { Header, Sidebar, Field } from "./components";

function App() {
  const dispatch = useDispatch();
  const { reports, size, loading } = useSelector((state) => state.main);
  const modes = useSelector(selectModes);

  useEffect(() => {
    dispatch(getModesAsync());
  }, []);

  let body = null;
  if (loading) {
    body = <h2>Loading...</h2>;
  } else {
    body = (
      <div className="mb-5">
        <h2>Starnavi test</h2>
        <Header modes={modes} />

        <FieldWrap className="mt-3">
          <div className="row align-items-start">
            <Field size={size} />
            <Sidebar reports={reports} />
          </div>
        </FieldWrap>
      </div>
    );
  }

  return (
    <div className="App">
      <Wrap className="container d-flex align-items-center justify-content-center">
        {body}
      </Wrap>
    </div>
  );
}

export default memo(App);
